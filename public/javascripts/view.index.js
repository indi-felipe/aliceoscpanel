if(navigator.userAgent.match(/iPad/i) != null) {
	document.addEventListener('touchmove', function(e) {
		e.preventDefault();
	});
}

$(function () {
	/* REFRESH LOGIC */
	var refresh =  document.querySelector('[data-refresh]');
	var refreshData = document.querySelector('[data-reload]');
	
	refreshData.value = location.host;
	refresh.addEventListener('click', function() {
		setTimeout(function() {
			location.href = refreshData.value.includes('http://') ? refreshData.value : 'http://' +refreshData.value;
		}, 10);
	});

	/* TIMLINE LOGIC */
	var timelineElements = Array.from(document.querySelectorAll('[data-time]'));
	var totalWidth = document.querySelector('[data-time="width"]').clientWidth;

	var currentTime = document.querySelector('[data-time="current"]');
	var currentTotal = document.querySelector('[data-time="total"]');
	var progress = document.querySelector('[data-time="progress"]');
	
	timelineElements.map(function(x) {
		switch(x.dataset.time) {
		case 'progress':
			$(x).css('width', getProgress(0, 10, totalWidth) + 'px');
			break; 
		}
	});

	window.addEventListener('resize', function() {
		totalWidth = document.querySelector('[data-time="width"]').clientWidth;
	});

	function getProgress(currTime, totalTime, totalWidth) {
		return currTime * totalWidth / totalTime;
	}
	
	//FastClick.attach(document.body);
	var attachFastClick = Origami.fastclick;
	attachFastClick(document.body);
	
	var socket = io.connect('/');
	
	/* PLAY PAUSE STATE HANDLING */
	var currentTimeControl, pastTime;
	var icon = $('#playPause').children()[0];
	var text = $('#playPause').find('.inner-text')[0];
	
	socket.on('servermessage', $.throttle(50, function (data) {
		if(data.message) {
			
			currentTimeControl = data.message.currentTime.value;
			
			if(pastTime && pastTime != currentTimeControl) {
				// validate if video time is changing, if it is, it means video is playing, so the message should be to pause
				if(icon.classList.contains('glyphicon-play')) {
					// update DOM elements
					icon.classList.add('glyphicon-pause');
					icon.classList.remove('glyphicon-play');
					text.textContent = ' Pause';
				}
			} else {
				// validate if video time is changing, if it isn't, it means video is paused, so the message should be to play
				if(icon.classList.contains('glyphicon-pause')) {
					// update DOM elements
					icon.classList.add('glyphicon-play');
					icon.classList.remove('glyphicon-pause');
					text.textContent = ' Play';
				}
			}
			
			var currTime = data.message.currentTime.value;
			var currTotal = data.message.totalTime.value;

			var cMinutes = Math.floor(currTime / 60);
			var tMinutes = Math.floor(currTotal / 60);

			var cSeconds = (currTime % 60);
			var tSeconds = (currTotal % 60);

			var cMills = Math.floor((currTime - Math.floor(currTime)) * 100);
			var tMills = Math.floor((currTotal - Math.floor(currTotal)) * 100);

			if(cMills < 10) cMills = '0' + cMills;
			if(tMills < 10) tMills = '0' + tMills;

			$(progress).css('width', getProgress(currTime, currTotal, totalWidth) + 'px');
			currentTime.textContent = (cMinutes > 10 ? cMinutes : '0'+ cMinutes ) + ':' +  (cSeconds < 10 ? '0' + parseInt(cSeconds) + ':' + cMills :  parseInt(currTime) + ':' + cMills);
			currentTotal.textContent = (tMinutes > 10 ? tMinutes : '0'+ tMinutes ) + ':' +  (tSeconds < 10 ? '0' + parseInt(tSeconds) + ':' + tMills : parseInt(currTotal) + ':' + tMills);
			pastTime = currentTimeControl;
		}
	}));
	
	$(':button').on( 'click', function() {
		socket.emit('clientclick', { message: this.id });
	});
	
	// With JQuery
	$('#volume').slider({step: 1, min: 0, max: 150, value: 50});

	$('#volume').on( 'slidechange', function() {
		//alert(ui.value);
		socket.emit('clientchange', { message: this.id, valor: this.value});
	} );

});