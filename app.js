/* **** **** **** **** */
/* IMPORTANT VARIABLES */
/* **** **** **** **** */
// connection data for dimmer
var LIGHTS = {
	port: 12345, //8085
	host: '192.168.1.253' // all hosts must be a string
};

// connection with millumin mapping
var MAPPING = {
	inport: 8083,
	outport: 8084,
	host: '192.168.1.213',
	launchColumn: 'millumin/action/launchColumn',
	currentTime: '/millumin/index:1/media/time',
	playOrPauseColumn: 'millumin/index:1/startOrPauseMedia'
};

// connection with interactive
var INTERACTIVE = {
	address: 'alice/interaction',
	receivers: [
		{
			name: 'Periscope',
			port: 8085,
			host: '192.168.1.198'
		},
		{
			name: 'Multi 1',
			port: 8086,
			host: '192.168.1.213'
		},
		{
			name: 'Multi 2',
			port: 8087,
			host: '192.168.100.7'
		},
		{
			name: 'Multi 3',
			port: 8088,
			host: '192.168.100.8'
		}
	]
};

// sound control
var SOUND = {
	host: '192.168.1.213', // Should be same as MAPPING
	port: 8787
};

console.log('LIGHT CONNECTION DATA: ' + JSON.stringify(LIGHTS));
console.log('MAPPING CONNECTION DATA: ' + JSON.stringify(MAPPING));
console.log('INTERACTIVE CONNECTION DATA: ' + JSON.stringify(INTERACTIVE));
console.log('SOUND CONNECTION DATA: ' + JSON.stringify(SOUND));

/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var osc = require ('osc-min');
var udp = require('dgram');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'vash');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

// create web server
var server = http.createServer(app);
server.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
});


// -------------------------------------------------------------------------
// create socket.io (browser comunication)
// -------------------------------------------------------------------------
var io = require('socket.io').listen(server);
var oscManager;
//io.set('log level', 1);
io.sockets.on('connection', function (socket) {
	console.log('--- SERVER: io.sockets.on connection (1 client connected)');
    //socket.emit('servermessage', { message: 'welcome to the chat' });
    
	if(!oscManager) {
		oscManager = new OSCManager(socket);
	} else {
		oscManager.sockudp.close();
		oscManager = new OSCManager(socket);
	}
	
	socket.on('clientclick', function (data) {
        
		if(data.message) {
			console.log('SERVER: click = ' + data.message);
			//oscManager.send(data.message, data.message);

			switch(data.message) {
			case 'LIGHTS_ON':
				turnOnLights();
				break;
			case 'LIGHTS_OFF':
				turnOffLights();
				break;
			case 'VIDEO_ENG':
				oscManager.send(MAPPING.launchColumn, 1);
				break;
			case 'VIDEO_FR':
				oscManager.send(MAPPING.launchColumn, 2);
				break;
			case 'SCREENSAVER':
				oscManager.send(MAPPING.launchColumn, 3);
				break;
			case 'MOCKUP_OFF':
				oscManager.send(MAPPING.launchColumn, 4);
				break;
			case 'INTERACTIVE_ON':
				oscManager.sendMultiples(INTERACTIVE.address, INTERACTIVE.receivers, 1);
				break;
			case 'INTERACTIVE_OFF': 
				oscManager.sendMultiples(INTERACTIVE.address, INTERACTIVE.receivers, 0);
				break;
			case 'vMas': 
				oscManager.sendUdp('up');
				break;
			case 'vMenos': 
				oscManager.sendUdp('down');
				break;
			case 'REFRESH':
				oscManager.send(MAPPING.launchColumn, 4);
				break;
			case 'playPause':
				oscManager.send(MAPPING.playOrPauseColumn);
				break;
			}

		}
		//io.sockets.emit('message', data);
	});
    
	socket.on('clientchange', function (data) {
        
		if(data.message) {
			console.log('SERVER: change = ' + data.message + ' ' + data.valor);
			oscManager.send(data.message, data.valor);
		}
		//io.sockets.emit('message', data);
	});

	socket.on('disconnect', function(data) {
		console.log('close!', data);
		// io = require('socket.io').listen(server);
	});
	
});

// -------------------------------------------------------------------------
// OSC comunication
// -------------------------------------------------------------------------

function OSCManager(parent) {
	this.parent = parent;
	this.sockudp = udp.createSocket('udp4');

	this.sockudp.on('message', function (msg) {
		var message = osc.fromBuffer(msg);
		message.elements.map(function (d) {
			if(d.address === MAPPING.currentTime) {
				parent.emit('servermessage', { message: {currentTime: d.args[0], totalTime: d.args[1]} } );
			}
		});
	});
	
	this.sockudp.bind(MAPPING.inport);
	
	this.send = function (id,val) {
		var buf;
		var data = (!val ? {address: '/' + id} : {address: '/' + id, args: [val]});
		buf = osc.toBuffer(data);
		this.sockudp.send(buf, 0, buf.length, MAPPING.outport, MAPPING.host, function(err) {
			if (err) console.log(err);
		});
	};

	this.sendMultiples = function(address, receivers, value) {
		var buf;
		buf = osc.toBuffer({
			address: '/'+ address,
			args: [value]
		});
		var that = this;
		receivers.map( function(x) {
			that.sockudp.send(buf, 0, buf.length, x.port, x.host, function(err) {
				if (err) console.log(err);
			});
		});
	};

	this.sendUdp = function(val) {
		var vol = Buffer.from(val);
		this.sockudp.send(vol, 0, vol.length, SOUND.port, SOUND.host, function(err) {
			if (err) console.log(err);
		});
	};
	
}


/**
 * Simple udp manager
 */

var simpleUdp = udp.createSocket('udp4');
simpleUdp.bind(LIGHTS.port);

var mess1 = Buffer.from('FFE0FF');
var mess2 = Buffer.from('FFE000');

function turnOnLights () {
	simpleUdp.send(mess1, 0, mess1.length, LIGHTS.port, LIGHTS.host, function(err) {
		if(err) throw err;
		//simpleUdp.close(); // -> this crashes app
	});
}
function turnOffLights () {
	simpleUdp.send(mess2, 0 , mess2.length,  LIGHTS.port, LIGHTS.host, function(err) {
		if(err) throw err;
		//simpleUdp.close(); // -> this crashes app
	});
}